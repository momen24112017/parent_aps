<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function listUsers(Request $request){
        
        $jsonStringDataProviderX = file_get_contents(base_path('resources/json/DataProviderX.json'));
        $jsonStringDataProviderY = file_get_contents(base_path('resources/json/DataProviderY.json'));
        $arrDataProviderX = json_decode($jsonStringDataProviderX, true);
        $arrDataProviderY = json_decode($jsonStringDataProviderY, true);
        //dd($arrDataProviderX);
        $arrData = array();
        if(isset($request['provider'])){
            if($request['provider']=='DataProviderX'){
                $arrUser = $arrDataProviderX;
            }else if($request['provider']=='DataProviderY'){
                $arrUser = $arrDataProviderY;
            }else{
                $arrUser['users'] = array();
            }
        
        }else{
            $arrUser = array_merge_recursive( $arrDataProviderX, $arrDataProviderY);
            
        }
        if(isset($request['statusCode'])){
            // dd('hello');
            //$arrUser = array_merge_recursive( $arrDataProviderX, $arrDataProviderY);    
            $arrResult['users'] = array();
            foreach($arrUser['users'] as $obj){
                // dd($obj);
                if($request['statusCode']=='authorized'){
                   
                    if(isset($obj['status']) && $obj['status']==100 || isset($obj['statusCode']) && $obj['statusCode']==1){
                        $arrResult['users'][] = $obj; 
                    }
                }else if($request['statusCode']=='declined'){
                    if(isset($obj['status']) && $obj['status']==200||isset($obj['statusCode']) && $obj['statusCode']==2){
                        $arrResult['users'][] = $obj; 
                    }
                }else if($request['statusCode']=='declined'){
                    if(isset($obj['status']) && $obj['status']==300||isset($obj['statusCode']) && $obj['statusCode']==3){
                        $arrResult['users'][] = $obj; 
                    }
                }else{
                    $arrResult['users'] = array();
                }   
            }
            $arrUser = $arrResult;
        }
        if(isset($request['balanceMin']) && isset($request['balanceMax'])){
            $arrResult['users'] = array();
            foreach($arrUser['users'] as $obj){
                if(isset($obj['parentAmount']) && $obj['parentAmount']>=$request['balanceMin'] && $obj['parentAmount']<=$request['balanceMax'] || isset($obj['balance']) && $obj['balance'] >=$request['balanceMin'] && $obj['balance']<=$request['balanceMax']){
                    $arrResult['users'][] = $obj;
                }
            }
            $arrUser = $arrResult;
        }
        if(isset($request['currency'])){
            $arrResult['users'] = array();
            foreach($arrUser['users'] as $obj){
                if(isset($obj['currency']) && $obj['currency']==$request['currency']  || isset($obj['Currency']) && $obj['Currency'] ==$request['currency']){
                    $arrResult['users'][] = $obj;
                }
            }
            $arrUser = $arrResult;
        }
        // dd($arrUser);
        $arrData['totalNumberofUsers'] = count($arrUser['users']);
        $arrData['users'] = $arrUser['users'];
        return $arrData;

    }
   
}
